from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "index.html"


class ContactsView(TemplateView):
    template_name = "about.html"

class DairyView(TemplateView):
    template_name = "dairy.html"

class LoginView(TemplateView):
    template_name = "login.html"

class ProfileView(TemplateView):
    template_name = "profile.html"